﻿(function (cibertec) {
    cibertec.video = {
        videoElement: document.getElementById("video"),
        play: function () {
            if (this.videoElement.paused) {
                this.videoElement.play();
            }
        },
        stop: function () {
            this.videoElement.currentTime = 0;
        },
        pause: function () {
            if (this.videoElement.played) {
                this.videoElement.pause();
            }
        }
    };
    console.log(cibertec);
}
)(window.cibertec = window.cibertec || {});