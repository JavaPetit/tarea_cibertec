﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc.Controllers
{
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unit;

        public ProductController(
            IUnitOfWork unit
            )
        {
            _unit = unit;
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
        }

        // GET: Order
        public ActionResult Index()
        {
            return View(_unit.Products.GetList());
        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(Products product)
        {
            if (ModelState.IsValid)
            {
                _unit.Products.Insert(product);
                return RedirectToAction("Index");
            }

            return View(product);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(_unit.Products.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Products product)
        {
            if (_unit.Products.Update(product))
            {
                return RedirectToAction("Index");
            }
            return View(product);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(_unit.Products.GetById(id));
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Products.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.Products.GetById(id));
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(_unit.Products.GetById(id));
        }
    }
}