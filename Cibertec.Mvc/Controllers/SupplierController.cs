﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc.Controllers
{
    public class SupplierController : Controller
    {

        private readonly IUnitOfWork _unit;

        public SupplierController(
            IUnitOfWork unit
            )
        {
            _unit = unit;
            //_unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ToString());
        }

        // GET: Order
        public ActionResult Index()
        {
            return View(_unit.Suppliers.GetList());
        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(Suppliers supplier)
        {
            if (ModelState.IsValid)
            {
                _unit.Suppliers.Insert(supplier);
                return RedirectToAction("Index");
            }

            return View(supplier);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Suppliers supplier)
        {
            if (_unit.Suppliers.Update(supplier))
            {
                return RedirectToAction("Index");
            }
            return View(supplier);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Suppliers.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.Suppliers.GetById(id));
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(_unit.Suppliers.GetById(id));
        }


    }
}