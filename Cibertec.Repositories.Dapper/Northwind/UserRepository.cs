﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(string connection) : base(connection) { }


        public bool Delete(string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM User " +
                    "where Email = @email", new
                    {
                        email = email
                    });
                return Convert.ToBoolean(result);
            }
        }


        public User GetByEmail(string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<User>().Where(user => user.Email.Equals(email)).First();
            }
        }

        public new bool Update(Customers customers)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update User " +
                    "set Email = @company," +
                    "FirstName = @contact," +
                    "LastName = @city," +
                    "Password = @country," +
                    "Phone = @phone " +
                    "where CustomerId = @myId", new
                    {
                        company = customers.CompanyName,
                        contact = customers.ContactName,
                        city = customers.City,
                        country = customers.Country,
                        phone = customers.Phone,
                        myId = customers.CustomerId

                    });

                return Convert.ToBoolean(result);
            }
        }


    }
}
