﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class ProductRepository : Repository<Products>, IProductRepository
    {
        public ProductRepository(string connection) : base(connection) { }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM Products " +
                    "where ProductID = @myId", new
                    {
                        myId = id
                    });
                return Convert.ToBoolean(result);
            }
        }


        public new Products GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Products>().Where(product => product.ProductId.Equals(id)).First();
            }
        }

        public new bool Update(Products product)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update Products " +
                    "set ProductName = @productName," +
                    "SupplierId = @supplierId," +
                    "CategoryId = @categoryId," +
                    "UnitPrice = @unitPrice," +
                    "QuantityPerUnit = @quantityPerUnit," +
                    "Discontinued = @discontinued " +
                    "where ProductID = @myId", new
                    {
                        productName = product.ProductName,
                        supplierId = product.SupplierId,
                        categoryId = product.CategoryId,
                        unitPrice = product.UnitPrice,
                        quantityPerUnit = product.QuantityPerUnit,
                        discontinued = product.Discontinued,
                        myId = product.ProductId

                    });

                return Convert.ToBoolean(result);
            }
        }


        public new int Insert(Products product)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "INSERT INTO Products(ProductName, SupplierId, CategoryId, UnitPrice, QuantityPerUnit, Discontinued) VALUES (" +
                    "@productName," +
                    "@supplierId," +
                    "@categoryId," +
                    "@unitPrice," +
                    "@quantityPerUnit," +
                    "@discontinued )", new
                    {
                        productName = product.ProductName,
                        supplierId = product.SupplierId,
                        categoryId = product.CategoryId,
                        unitPrice = product.UnitPrice,
                        quantityPerUnit = product.QuantityPerUnit,
                        discontinued = product.Discontinued

                    });

                return result;
            }
        }

        public new IEnumerable<Products> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Products>().OrderByDescending(m => m.ProductId);
            }
        }
    }
}
