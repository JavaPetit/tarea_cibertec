﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class SupplierRepository : Repository<Suppliers>, ISupplierRepository
    {
        public SupplierRepository(string connection) : base(connection) { }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM Suppliers " +
                    "where SupplierID = @myId", new
                    {
                        myId = id
                    });
                return Convert.ToBoolean(result);
            }
        }


        public new Suppliers GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Suppliers>().Where(supplier => supplier.SupplierId.Equals(id)).First();
            }
        }

        public new bool Update(Suppliers supplier)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update Suppliers " +
                    "set CompanyName = @company," +
                    "ContactName = @contact," +
                    "ContactTitle = @contactTitle," +
                    "City = @city," +
                    "Country = @country," +
                    "Phone = @phone, " +
                    "Fax = @fax " +
                    "where SupplierID = @myId", new
                    {
                        company = supplier.CompanyName,
                        contact = supplier.ContactName,
                        contactTitle = supplier.ContactTitle,
                        city = supplier.City,
                        country = supplier.Country,
                        phone = supplier.Phone,
                        fax = supplier.Fax,
                        myId = supplier.SupplierId

                    });

                return Convert.ToBoolean(result);
            }
        }

        public new int Insert(Suppliers supplier)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "INSERT INTO Suppliers (CompanyName, ContactName, ContactTitle, City, Country, Phone, Fax ) VALUES ( " +
                    "@company," +
                    "@contact," +
                    "@contactTitle," +
                    "@city," +
                    "@country," +
                    "@phone, " +
                    "@fax )" , new
                    {
                        company = supplier.CompanyName,
                        contact = supplier.ContactName,
                        contactTitle = supplier.ContactTitle,
                        city = supplier.City,
                        country = supplier.Country,
                        phone = supplier.Phone,
                        fax = supplier.Fax

                    });

                return result;
            }
        }

        public new IEnumerable<Suppliers> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Suppliers>().OrderByDescending(m => m.SupplierId);
            }
        }

    }
}
