﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class EmployeeRepository : Repository<Employees>, IEmployeeRepository
    {

        public EmployeeRepository(string connection) : base(connection) { }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM Employees " +
                    "where EmployeeID = @myId", new
                    {
                        myId = id
                    });
                return Convert.ToBoolean(result);
            }
        }

        public Employees GetbyId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Employees>().Where(employee => employee.EmployeeId.Equals(id)).First();
            }
        }

        public new bool Update(Employees employee)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update Employees " +
                    "set FirstName = @firstName," +
                    "LastName = @lastName," +
                    "BirthDate = @birthDate," +
                    "City = @city," +
                    "ReportsTo = @reportsTo " +
                    "where EmployeeId = @myId", new
                    {
                        firstName = employee.FirstName,
                        lastName = employee.LastName,
                        birthDate = employee.BirthDate,
                        city = employee.City,
                        reportsTo = employee.ReportsTo,
                        myId = employee.EmployeeId

                    });

                return Convert.ToBoolean(result);
            }
        }
    }
}
