﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class OrderRepository : Repository<Orders>, IOrderRepository
    {
        public OrderRepository(string connection) : base(connection) { }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM Orders " +
                    "where OrderID = @myId", new
                    {
                        myId = id
                    });
                return Convert.ToBoolean(result);
            }
        }


        public new Orders GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Orders>().Where(order => order.OrderId.Equals(id)).First();
            }
        }

        public new bool Update(Orders order)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update Orders " +
                    "set CustomerId = @customerId," +
                    "EmployeeId = @employeeId," +
                    "OrderDate = @orderDate," +
                    "RequiredDate = @requiredDate," +
                    "ShippedDate = @shippedDate," +
                    "ShipVia = @shipVia, " +
                    "Freight = @freight, " +
                    "ShipName = @shipName " +
                    "WHERE OrderID = @myId", new
                    {
                        customerId = order.CustomerId,
                        employeeId = order.EmployeeId,
                        orderDate = order.OrderDate,
                        requiredDate = order.RequiredDate,
                        shippedDate = order.ShippedDate,
                        shipVia = order.ShipVia,
                        freight = order.Freight,
                        shipName = order.ShipName,
                        myId = order.OrderId

                    });

                return Convert.ToBoolean(result);
            }
        }

        public new int Insert(Orders order)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "INSERT INTO Orders (CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName) VALUES(" +
                    "@customerId," +
                    "@employeeId," +
                    "@orderDate, " +
                    " @requiredDate," +
                    "@shippedDate," +
                    "@shipVia, " +
                    "@freight, " +
                    "@shipName )" , new
                    {
                        customerId = order.CustomerId,
                        employeeId = order.EmployeeId,
                        orderDate = order.OrderDate,
                        requiredDate = order.RequiredDate,
                        shippedDate = order.ShippedDate,
                        shipVia = order.ShipVia,
                        freight = order.Freight,
                        shipName = order.ShipName

                    });

                return result;
            }
        }

        public new IEnumerable<Orders> GetList() {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Orders>().OrderByDescending(m => m.OrderId);
            }
        }
    }
}
