﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class CustomerRepository : Repository<Customers>, ICustomerRepository
    {
        public CustomerRepository(string connection) : base(connection) { }

        public bool Delete(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("DELETE FROM Customers " +
                    "where CustomerID = @myId", new
                    {
                        myId = id
                    });
                return Convert.ToBoolean(result);
            }
        }

        public Customers GetbyId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Customers>().Where(customer => customer.CustomerId.Equals(id)).First();
            }
        }

        public new bool Update(Customers customers)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("" +
                    "update Customers " +
                    "set CompanyName = @company," +
                    "ContactName = @contact," +
                    "City = @city," +
                    "Country = @country," +
                    "Phone = @phone " +
                    "where CustomerId = @myId", new
                    {
                        company = customers.CompanyName,
                        contact = customers.ContactName,
                        city = customers.City,
                        country = customers.Country,
                        phone = customers.Phone,
                        myId = customers.CustomerId

                    });

                return Convert.ToBoolean(result);
            }
        }
    }
}
